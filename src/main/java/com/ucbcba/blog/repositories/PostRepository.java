package com.ucbcba.blog.repositories;

import com.ucbcba.blog.entities.Post;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface PostRepository extends CrudRepository<Post, Integer> {

}