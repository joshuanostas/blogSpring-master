package com.ucbcba.blog.controllers;

import com.ucbcba.blog.entities.Comment;
import com.ucbcba.blog.entities.Post;
import com.ucbcba.blog.services.CommentService;
import com.ucbcba.blog.services.PostService;
import com.ucbcba.blog.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommentController {

    private CommentService commentService;

    private UserService userService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setUserService(UserService userService){this.userService=userService;}

    /**
     * List all posts.
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public String save(Comment comment, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            model.addAttribute("users",userService.listAllUsers());
            return "redirect:/post/"+comment.getPost().getId();
        }
        commentService.saveComment(comment);
        return "redirect:/post/"+comment.getPost().getId();
    }


}
