package com.ucbcba.blog.entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Size(min=1,max = 50,message = "Debe ser mayor a 1 y menor a 50")
    private String text;

    @NotNull
    @ManyToOne
    @JoinColumn(name="post_id")
    private Post post;

    @NotNull
    @Column(columnDefinition="int(5) default '0'")
    private Integer likes = 0 ;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull(message = "can't be empty")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date date;

    public Comment(){

    }
    public Comment(Post post){
        this.post = post;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
